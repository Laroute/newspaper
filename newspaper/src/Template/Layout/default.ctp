<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = "J'❤ le val de saire";
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('styleNewspaper.css') ?>

    <?= $this->Html->script('bootstrap.min.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>
<body>

<header>
    <nav class="navbar navbar-expand-lg navbar-light navbar-perso">
        <?= $this->Html->link("J'❤ le val de saire", ['controller'=>'Articles','action' => 'index']) ?>
    </nav>
    <?= $this->Flash->render() ?>
</header>
  
   
    <div class="container-fluid">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    <p>© 2019  <?= $this->Html->link("Administrateur", ['controller'=>'Users','action' => 'index']) ?> </p>
    </footer>
</body>
</html>
