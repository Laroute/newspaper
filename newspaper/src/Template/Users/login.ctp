<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="offset-md-3 medium-6">
<?= $this->Flash->render('auth') ?>
    <?= $this->Form->create() ?>
    <fieldset class="form-group">
        <?= $this->Form->control('username', ['class' => 'form-control', 'label' => [
                    'text' => 'Nom d\'utilisateur'
                ]]) ?>
        <?= $this->Form->control('password', ['class' => 'form-control', 'label' => [
                 'text' => 'Mot de Passe'
                ]]) ?>
    </fieldset>
    <?= $this->Form->button(__('Se connecter'),['class'=>'btn btn-success btn-lg','role' =>'button','aria-disabled'=>'true' ]); ?>
    <?= $this->Form->end() ?>
</div>