<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="offset-md-3 medium-6">
    <h3 class="admin-titre" ><?= $state?> un utilisateur</h3>

    <fieldset>
        <?php
            echo $this->Form->create($user);
            echo $this->Form->control('username', [
                'label' => [
                    'text' => 'Nom d\'utilisateur'
                ]
            ]);
            echo $this->Form->control('password', [
                'label' => [
                    'text' => 'Mot de Passe'
                ]
            ]);
        ?>
    </fieldset>
    <?= $this->Form->button('Valider',['class'=>'btn btn-success btn-lg','role' =>'button','aria-disabled'=>'true' ]) ?> 
    <?php
    if($admin === 1) { 
        echo $this->Html->link("Retour", ['action' => 'view'],['class'=>'btn btn-light btn-lg','role' =>'button','aria-disabled'=>'true' ]) ;
        
    } else {
       echo  $this->Html->link("Retour", ['action' => 'index'],['class'=>'btn btn-light btn-lg','role' =>'button','aria-disabled'=>'true' ]) ;   } ?>
    <?= $this->Form->end() ?>
</div>
