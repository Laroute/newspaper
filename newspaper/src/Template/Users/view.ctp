<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="offset-md-3 medium-6">
    <h3 class="admin-titre" >Gestion des utilisateurs</h3>
    <table  class="table table-center table-hover">
        <thead>
            <tr>

                <th scope="col"><?= $this->Paginator->sort('Utilisateurs') ?></th>
                <th scope="col" class="actions">Modifier</th>
                <th scope="col" class="actions">Supprimer</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= h($user->username) ?></td>
                <td><?= $this->Html->link($this->Html->image('icon/edit.png',['class'=>'icon']), ['action' => 'edit', $user->id],['escape'=>false]) ?></td>
                <td><?= $this->Form->postLink($this->Html->image('icon/trash.png',['class'=>'icon']), ['action' => 'delete', $user->id], ['confirm' => __('Êtes-vous sûre de supprimer {0} ?', $user->username),'escape'=>false]) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="container admin_menu">
    <?= $this->Html->link("Ajouter un utilisateur ", ['controller'=>'users','action' => 'add'],['class'=>'btn btn-light btn-lg','role' =>'button','aria-disabled'=>'true' ]) ?>
    <?= $this->Html->link("Retour au menu", ['controller'=>'users','action' => 'index'],['class'=>'btn btn-success btn-lg','role' =>'button','aria-disabled'=>'true' ]) ?>
</div>
</div>
