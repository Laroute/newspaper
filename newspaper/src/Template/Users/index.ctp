<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="container admin_menu">
    <?= $this->Html->link("Accéder aux articles", ['controller'=>'articles','action' => 'views'],['class'=>'btn btn-light btn-lg','role' =>'button','aria-disabled'=>'true' ]) ?>
   
    <?php
    if($admin === 1) { 
        echo $this->Html->link("Accéder aux utilisateurs", ['controller'=>'users','action' => 'view'],['class'=>'btn btn-light btn-lg','role' =>'button','aria-disabled'=>'true' ]);
    } else{
        echo $this->Html->link("Modifier son profil", ['controller'=>'users','action' => 'edit', $admin],['class'=>'btn btn-light btn-lg','role' =>'button','aria-disabled'=>'true' ]);
    } ?>
    
    <?= $this->Html->link("Retour au site", ['controller'=>'articles','action' => 'index'],['class'=>'btn btn-success btn-lg','role' =>'button','aria-disabled'=>'true' ]) ?>
    <?= $this->Html->link("Déconnexion", [ 'action' => 'logout'],['class'=>'btn btn-danger btn-lg','role' =>'button','aria-disabled'=>'true' ]) ?>
</div>