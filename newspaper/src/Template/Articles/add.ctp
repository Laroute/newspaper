<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Article $article
 */
?>
<div class="offset-md-3 medium-6">
<h3 class="admin-titre" ><?= $state?> un article</h3>
<?php
    echo $this->Form->create($article,array('enctype'=>'multipart/form-data'));
    echo $this->Form->control('title', [
        'label' => [
            'text' => 'Titre de l\'article'
        ]
    ]);
    echo $this->Form->control('text', ['rows' => '15'  ,'label' => [
        'text' => 'Texte de l\'article'
    ]
]);
    echo $this->Form->control('author', [
        'label' => [
            'text' => 'Auteur de l\'article'
        ]
    ]);

    echo $this->Form->input('upload', array('type'=>'file',   'label' => [
        'text' => 'Image de l\'article'
    ]));
    echo '<div class="div-btn-center" >';
    echo $this->Html->link("Retour", ['action' => 'views'],['class'=>'btn btn-light btn-lg','role' =>'button','aria-disabled'=>'true' ]) ;
    echo $this->Form->button(__('Valider'),['class'=>'btn btn-success btn-center btn-lg','role' =>'button','aria-disabled'=>'true' ]);
    echo '</div>';
    echo $this->Form->end();  
?>
</div>