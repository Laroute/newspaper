<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Articles[]|\Cake\Collection\CollectionInterface $articles
 */
?>

<div class="offset-md-3 medium-6">
    <h3 class="admin-titre" ><?= $article->title?></h3>
</div>
    <div class="offset-md-4 medium-4 image-div-view">
     <?=  $this->Html->image($article->namepicture)?>
     <p class="text-article">
     <?= $article->text ?>
     <div style="text-align:right"><i>Publié le <?= $article->created->i18nFormat('dd/MM/yyyy')?> par <?= $article->author?>. </i></div>
     <div style="text-align:center;margin-top:20px;margin-bottom:20px;"><?= $this->Html->link("Retour à l'accueil", ['controller'=>'articles','action' => 'index'],['class'=>'btn btn-success btn-lg','role' =>'button','aria-disabled'=>'true' ]) ?>
     </div>
    <div>
</div>
</div>