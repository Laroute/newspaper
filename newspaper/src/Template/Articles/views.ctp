<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Articles[]|\Cake\Collection\CollectionInterface $articles
 */
?>
<div class="offset-md-2 medium-8">
    <h3 class="admin-titre" >Gestion des Articles</h3>
    <table  class="table table-center table-hover">
        <thead>
            <tr>

                <th scope="col">Titre</th>
                <th scope="col">Auteur</th>
                <th scope="col">Date</th>
                <th scope="col">Visualiser</th>
                <th scope="col">Modifier</th>
                <th scope="col">Supprimer</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($articles as $article): ?>
            <tr>
                <td><?= $article->title ?></td>
                <td><?= $article->author ?></td>
                <td><?= $article->created->i18nFormat('yyyy-MM-dd') ?></td>
                <td><?= $this->Html->link($this->Html->image('icon/view.png',['class'=>'icon']), ['action' => 'view', $article->id],['escape'=>false]) ?></td>
                <td><?= $this->Html->link($this->Html->image('icon/edit.png',['class'=>'icon']), ['action' => 'edit', $article->id],['escape'=>false]) ?></td>
                <td><?= $this->Form->postLink($this->Html->image('icon/trash.png',['class'=>'icon']), ['action' => 'delete', $article->id], ['confirm' => __('Êtes-vous sûre de supprimer {0} ?', $article->title),'escape'=>false]) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<<') ?>
            <?= $this->Paginator->prev('<') ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next('>') ?>
            <?= $this->Paginator->last('>>') ?>
        </ul>
    </div>   

    <div class="container admin_menu">
    <?= $this->Html->link("Ajouter un article ", ['controller'=>'articles','action' => 'add'],['class'=>'btn btn-light btn-lg','role' =>'button','aria-disabled'=>'true' ]) ?>
    <?= $this->Html->link("Retour au menu", ['controller'=>'users','action' => 'index'],['class'=>'btn btn-success btn-lg','role' =>'button','aria-disabled'=>'true' ]) ?>
</div>
</div>
