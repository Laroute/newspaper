<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="row">
    <?php foreach ($articles as $article): ?>
    <div class='offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-lg-3 col-lg-6 pic_div' >
        <div class='row article-div'>
            <div class='col-sm-3 div_picture'>
                <?php echo $this->Html->link($this->Html->image($article->namepicture), ['action' => 'view', $article->id],    array( 'escape' => false))  ?>
            </div>
                <div class='col-sm-9'>
                    <?php echo $this->Html->link($article->title, ['action' => 'view', $article->id])  ?>
                    <p> <?= $article->text?></p>
                    <span>Publié le <?= $article->created->i18nFormat('dd/MM/yyyy')?> par <?= $article->author?>. </span>
                </div>
          </div>
        </div>
     <?php endforeach; ?>
     <div class='offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-lg-3 col-lg-6' >
     <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<<') ?>
            <?= $this->Paginator->prev('<') ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next('>') ?>
            <?= $this->Paginator->last('>>') ?>
        </ul>
    </div>   
</div>
</div>

