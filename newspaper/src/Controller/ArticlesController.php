<?php
// src/Controller/ArticlesController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\Query;

class ArticlesController extends AppController
{
    public $paginate = [
        'limit' => 5,
        'order' => [
            'Articles.id' => 'desc'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Picture');
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
        $this->Auth->allow(['view', 'index']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->loadComponent('Paginator');
        $articles = $this->paginate($this->Articles->find());
        $this->set(compact('articles'));
    }
  
    /**
     * Views method for administration
     *
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function views()
    {
        $this->loadComponent('Paginator');
        if( $this->Auth->user('id') === 1){
            $articles = $this->paginate($this->Articles->find());
        } else {
            $articles = $this->paginate($this->Articles->find()->where(['user_id =' =>$this->Auth->user('id')]));
        }
        $this->set(compact('articles'));
    }

    /**
     * Views method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $article = $this->Articles->findById($id)->firstOrFail();
        $this->set(compact('article'));
    }
   
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {   
        $article = $this->Articles->newEntity();                
        if ($this->request->is('post')) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());         
            $article->namepicture = "null";
            $article->user_id = $this->Auth->user('id');

        if(!empty($this->request->data))
        {
            //Check if image has been uploaded
            if(!empty($this->request->data['upload']['name']))
            {
                $file = $this->request->data['upload']; //put the data into a var for easy use

                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions

                //only process if the extension is valid
                if(in_array($ext, $arr_ext))
                {
                    //do the actual uploading of the file. First arg is the tmp name, second arg is
                    //where we are putting it
                    move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/article/' . $file['name']);
                    $this->Picture->correctImageOrientation(WWW_ROOT . 'img/article/' . $file['name']);
                    $article->namepicture = 'article/'.$file['name'];
                }
            } 
           
        }
        if ($this->Articles->save($article)) {
            $this->Flash->success(__('Votre article a été sauvegardé.'));
            return $this->redirect(['action' => 'views']);
        }
        $this->Flash->error(__('Impossible d\'ajouter votre article.'));
        }
        $data = ['state' => 'Ajouter'];
        $this->set($data);

        $this->set('article', $article);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $article = $this->Articles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());



            if(!empty($this->request->data))
            {
                //Check if image has been uploaded
                if(!empty($this->request->data['upload']['name']))
                {
                    $file = $this->request->data['upload']; //put the data into a var for easy use

                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions

                    //only process if the extension is valid
                    if(in_array($ext, $arr_ext))
                    {
                        //do the actual uploading of the file. First arg is the tmp name, second arg is
                        //where we are putting it
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/article/' . $file['name']);
                        $this->Picture->correctImageOrientation(WWW_ROOT . 'img/article/' . $file['name']);
                        $article->namepicture = 'article/'.$file['name'];
                    }
                } 
            
            }

            if ($this->Articles->save($article)) {
                $this->Flash->success(__("L'article a été modifié."));
                return $this->redirect(['action' => 'views']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }

        $data = ['state' => 'Modifier'];
        $this->set($data);
        $this->set(compact('article'));
        $this->render('add');

    }
    
    /**
     * Delete method
     *
     * @param string|null $id Article id.
     * @return \Cake\Http\Response|null Redirects to view.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Articles->get($id);
        if ($this->Articles->delete($user)) {
            $this->Flash->success(__("L'article a bien été supprimé."));
            return $this->redirect(['action' => 'views']);
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }       
    }
    
}