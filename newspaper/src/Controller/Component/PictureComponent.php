<?php
namespace App\Controller\Component;

use Cake\Controller\Component;

class PictureComponent extends Component
{
    public  function correctImageOrientation($filename){
        ini_set('memory_limit', '-1');
        if (function_exists('exif_read_data')) {
        $exif = exif_read_data($filename);
        if($exif && isset($exif['Orientation'])) {
            $orientation = $exif['Orientation'];
            if($orientation != 1){
            $img = imagecreatefromjpeg($filename);
            $deg = 0;
            switch ($orientation) {
                case 3:
                $deg = 180;
                break;
                case 6:
                $deg = 270;
                break;
                case 8:
                $deg = 90;
                break;
            }
            if ($deg) {
                $img = imagerotate($img, $deg, 0);        
            }
            imagejpeg($img, $filename, 95);
            } 
        } 
        }
    }
}
