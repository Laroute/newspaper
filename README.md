Bienvenue sur mon site web créé en trois jours avec le framework cakePHP dans l'objectif de prouver mes compétences
Cette application permet la création, l'édition et la suppression d'article journalistique.
l'application permet aussi créé de nouveaux utilisateurs qui pourront créer eux-même d'autres articles.

La base de données est dans le fichier newspaper.sql situé dans le même repertoire que ce fichier.

Pour lancer l'application :
cd ./newspaper
bin/cake server

Accéder à
http://localhost:8765/articles pour la page accueil

http://localhost:8765/users pour la partie administration

il existe trois utilisateurs:
username | password

admin | admin
user1 | user1
user2 | user2

Admin peut visualiser tout les articles et posséde la gestion de tout les utilisateurs.

User ne peut visualiser que les articles qu 'il a créé et les modifier.
