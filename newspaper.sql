-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 03 oct. 2019 à 16:28
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `newspaper`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `text` text NOT NULL,
  `author` varchar(20) NOT NULL,
  `namepicture` varchar(40) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`id`, `user_id`, `title`, `text`, `author`, `namepicture`, `created`) VALUES
(1, 2, 'Le phare de Gatteville', 'Le phare de Gatteville, ou phare de Gatteville-Barfleur, est situé sur la pointe de Barfleur (commune de Gatteville-le-Phare), dans la Manche. Il signale les forts courants du raz de Barfleur.<br/><br/>\r\n\r\nLes courants forts au large de la pointe de Barfleur, et les nombreux naufrages (dont le plus célèbre est sans doute celui de la Blanche-Nef) rendent indispensable l\'édification d\'un phare.<br/><br/>\r\n\r\nEn 1774, sous le règne de Louis XVI, la Chambre de commerce de Rouen décide donc de faire construire un premier phare en granite, de 25 mètres. À son sommet, un feu de bois et de charbon brûlait continuellement. Le charbon (issu des houillères de Littry) était approvisionné à dos d\'homme et laissait peu de repos aux gardiens. En 1780 le feu à charbon fut remplacé par un système de réverbères constitué de 16 lampes à huile dans une lanterne vitrée.<br/><br/>\r\n\r\nCe phare étant trop petit pour recevoir les lentilles modernes, et trop faible pour pouvoir être exhaussé de 32 mètres, on décide en 1825 d\'ériger une nouvelle tour . L\'architecte et ingénieur des ponts et chaussées Charles-Félix Morice de la Rue, sous le règne de Charles X, qui dessinera ensuite le phare de la Hague, conçoit les plans du plus haut phare de l\'époque (dépassé depuis par le phare de l\'Île Vierge). La pose de la pierre centrale a lieu le 14 juin 1828 et les travaux s\'étaleront jusqu\'en 1835. C\'est en effet le 1er avril 1835 qu\'il fut allumé pour la première fois.<br/><br/>\r\n\r\nL\'ancien phare devient sémaphore.<br/><br/>\r\n\r\nCelui-ci est inscrit au titre des monuments historiques par arrêté du 11 mai 2009, tandis que le phare et les bâtiments annexes sont classés au même titre par arrêté le 19 juin suivant.', 'Victor La Route', 'article/f.jpg', '2019-10-03 14:45:23'),
(2, 2, 'Saint-Vaast-la-Hougue : le village préféré des Français est normand', 'Le village préféré des Français se trouve en Normandie. Saint-Vaast-la-Hougue a décroché ce titre à la fin de l\'émission présentée par Stéphane Bern mercredi soir sur France 3. <br/><br/>\r\n\r\nAprès les Hauts-de-France, la Normandie. Saint-Vaast-la-Hougue a été élu village préféré des Français à la fin de l\'émission de Stéphane Bern diffusée sur France 3 mercredi. C\'est la première fois qu\'une commune de Normandie gagne le titre depuis le lancement du programme en 2012.<br/><br/>\r\n\r\nNichée dans une baie paisible, Saint-Vaast-la-Hougue vit au rythme de son port de plaisance et son port de pêche, le principal du Cotentin. La station balnéaire de 1 700 habitants peut être admirée depuis le sommet de la tour d\'observation de Vauban, inscrite au patrimoine de l\'Unesco, située au bout de la presqu\'île. Sa tour jumelle se trouve non loin de là, sur l\'île de Tatihou, accessible en quelques minutes en bateau amphibie où à pied à marée basse. On peut y découvrir une réserve ornithologique, visiter un ancien lazaret, lieu de quarantaine pour les marins et marchandises au XVIIIème siècle durant l\'épidémie de peste, transformé en musée, et observer des plantes exotiques.<br/><br/>\r\n\r\nAprès ce titre, Saint-Vaast-la-Hougue peut espérer une augmentation du nombre de touristes, comme ça été le cas pour les villages gagnants des saisons précédentes. «Le village préféré a quelques avantages. Cassel [gagnant 2018] a eu 50% de visiteurs en plus, des papiers dans le \"New York Times\", ça a été une explosion touristique et économique. C\'est formidable quand une émission de service public peut dire qu\'elle dope l\'économie des territoires. Toutes les émissions ne peuvent pas en dire autant», s\'enthousiasmait Stéphane Bern auprès de Paris Match il y a quelques semaines.<br/><br/>\r\n\r\n«Le Village préféré des Français», diffusée pour la première fois cette année sur France 3, a permis de hisser la chaîne du service public à la première place des audiences de la soirée devant TF1 et France 2 avec 2,4 millions de téléspectateurs.<br/><br/>', 'MORGANE PRODUCTION', 'article/Saint-Vaast-la-Hougue.jpg', '2019-10-03 14:51:54'),
(3, 1, 'L\'île Tatihou', 'L\'île Tatihou est une île côtière française au nord-est du Cotentin située dans la rade de Saint-Vaast-la-Hougue (département de la Manche), commune à laquelle elle est administrativement rattachée.<br/><br/>\r\n\r\nD\'une superficie de 29 hectares, elle est accessible à pied à certaines marées basses. L\'île appartient au Conservatoire du littoral et n\'est pas habitée de manière permanente.<br/><br/>\r\nÀ partir de 1694, la tour de Tatihou et sa jumelle la Hougue (sur la côte à Saint-Vaast) sont construites par un élève de Vauban, Benjamin de Combes, afin de défendre la baie de Saint-Vaast contre les agresseurs. Les fortifications se poursuivront jusqu\'au xixe siècle. Ces tours sont inscrites, avec treize autres sites fortifiés par Vauban, au patrimoine mondial de l\'humanité le 7 juillet 2008.<br/><br/>\r\n\r\nEn 1721, la peste de Marseille incite le roi à créer un lazaret pour protéger le nord-ouest du royaume. L\'île Tatihou est alors choisie pour effectuer les quarantaines des équipages et des marchandises venant de la mer du Nord ou de la Méditerranée. Cet ensemble sanitaire fonctionne jusque dans les années 1860.<br/><br/>\r\n\r\nDe 1887 à 1923, le Muséum national d\'histoire naturelle, dont le siège est à Paris, tient sur l\'île sa toute première station maritime. Les chercheurs y travaillent sur l\'élevage du turbot en milieu artificiel, sur le plancton et sur les algues. Le laboratoire sera transféré à Saint-Servan en 192415 puis à Dinard en 1935 (l\'actuelle station de biologie marine de Dinard y est encore).', 'Wikipedia', 'article/Île_de_Tatihou.jpg', '2019-10-03 15:12:32'),
(4, 1, 'Le château de Tocqueville', 'Le château de Tocqueville est situé sur la commune de Tocqueville, dans le département de la Manche.<br/><br/>\r\n\r\nDemeure de la famille Clérel, seigneurs de Tocqueville, elle a notamment appartenu à Alexis de Tocqueville. Le château fait l\'objet de plusieurs protections au titre des monuments historiques.<br/><br/>\r\n\r\n\r\nLe château est édifié au xvie siècle et modifié en 17341.<br/><br/>\r\n\r\n\r\nEn 1833, Alexis de Tocqueville tombe sous le charme du château familial inhabité depuis un demi-siècle. À la mort de sa mère, il obtient qu\'il lui revienne plutôt qu\'à son frère Édouard et s\'y installe à partir de 1836. Sa femme dirige les travaux d\'aménagement et de restauration. L\'avenue est plantée en 1843, un bassin creusé en 1845 et un jardin anglais créé en 1856. Ce domaine lui permet d\'entamer une carrière politique dans la Manche en 1839.<br/><br/>\r\n\r\n\r\nEn 1894, on y adjoint un pavillon méridional de style Renaissance.<br/><br/>\r\n\r\n\r\nUn incendie se propage aux décors intérieurs du château en 1954, épargnant le chartrier et la bibliothèque d\'Alexis de Tocqueville, constitué par son aïeul, Bernard Bonaventure de Tocqueville, de 2 500 ouvrages des xviie et xviiie siècles.<br/><br/>\r\n\r\n\r\nLe 27 juin 1955, le bâtiment du xviiie siècle est inscrit au titre des monuments historiques, sa façade étant par la suite classée en même temps que celles des communs et de la maison du gardien, avec les toitures correspondantes, de même que les vestiges du pigeonnier, le 17 août 1979. Le 24 septembre 1965, ont été inscrits le hall d\'entrée, les deux salons du rez-de-chaussée et la chambre d\'Alexis de Tocqueville. Les façades et les toitures du reste du château, de la « menuiserie », de la « petite boulangerie » et de la « petite laiterie », l\'escalier, le chartrier, la bibliothèque, le portail d\'entrée, la balustrade et les escaliers fermant la cour des communs sud, ainsi que la fontaine du jardin nord avec son bassin, sont classés par arrêté du 17 décembre 1993. Le parc est inscrit le 30 octobre 2000.', 'Wikipedia', 'article/Tocqueville,_France-3.jpg', '2019-10-03 15:18:40'),
(5, 3, 'Barfleur', 'Barfleur est une commune française, située dans le nord-est du département de la Manche en région Normandie, peuplée de 577 habitants. <br/><br/>\r\n\r\nMaisons en granit, petits restaurants sur le port, église atypique… Rien ne vient rompre le charme de Barfleur. L\'église à la silhouette trapue, presque fortifiée, a la particularité d’avoir une chapelle d\'axe en dôme octogonal et un clocher sans flèche. Sur le quai, ne manquez pas la débarque des poissons, pour une immersion plus complète dans la vie et le rythme locaux. Dans les environs immédiats, se dressent l’emblématique sémaphore et l\'imposant phare de Gatteville, le deuxième plus haut phare de France. Le critique gastronomique Jean-Luc Petitrenaud ne s’y est pas trompé. Il est littérallement tombé sous le charme de Barfleur. <br/><br/>\r\n\r\nSi vous vous posiez la question \"où manger de bonnes moules frites en Normandie\", la réponse à votre question est peut-être là ! La grande majorité des moules disponibles dans le commerce provient de l\'élevage. Mais il existe des gisements naturels, dans l\'Est du Cotentin : les moules de Barfleur, de Ravenoville. Elles sont pêchées par des navires. Celle que l’on surnomme la « blonde de Barfleur » bénéficie depuis plusieurs années de la charte de qualité « moule de Barfleur Normandie fraîcheur mer». Pour la déguster, quel plus beau décor que les petits restaurants de Barfleur faisant face au port ? Cuisinées à la crème, marinière ou à la normande, c\'est le plat incontournable de l’été ! Avant de vous mettre l\'eau à la bouche, vérifiez auprès des restaurateurs la saison exacte de pêche des moules de Barfleur, étant donné qu\'il s\'agit de gisements naturels.', 'WikiVictor', 'article/barfleur.jpg', '2019-10-03 15:26:00'),
(6, 3, 'Bienvenue sur J\'aime le val de saire', 'J\'aime le val de saire est un site dédié aux amoureux du Val-de-Saire. Celui-ci regroupe les coins les plus beaux à visiter dans le Val-de-Saire. <br/><br/>Rejoignez nous sur le groupe facebook <a href=\"https://www.facebook.com/groups/1612414482328210/\" >J\'aime le val de saire</a>.', 'Victor La Route', 'article/valdesaire.jpg', '2019-10-03 15:43:22');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', '$2y$10$XLiLOG0nWMFaGHPFWc2dZeq8wlkPvecjNw0DxKPtGd49i1dKIGfIi'),
(2, 'user1', '$2y$10$xLlWAssA2jzBT1eFOwIAmO5Oph4IArZF7RJn7lnCDXgcNojXGyxwi'),
(3, 'user2', '$2y$10$Glsx/j4CD.yaLvCa96A2qeriPqCvULhdkF16vjNkLMjdD0SrSTT8i');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
